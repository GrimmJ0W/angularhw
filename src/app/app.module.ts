import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { ReactiveFormsModule} from "@angular/forms";
import {BrowserAnimationsModuleConfig} from "@angular/platform-browser/animations";

import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatChipsModule} from "@angular/material/chips";
import {MatSelectModule} from "@angular/material/select";

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Hw1Component } from './components/hw1/hw1.component';
import { Hw2Component } from './components/hw2/hw2.component';



@NgModule({
  declarations: [
    AppComponent,
    Hw1Component,
    Hw2Component

  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatChipsModule,
    MatButtonModule,
    MatSelectModule,
    MatCheckboxModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
