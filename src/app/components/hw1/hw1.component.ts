import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-hw1',
  templateUrl: './hw1.component.html',
  styleUrls: ['./hw1.component.css']
})
export class Hw1Component implements OnInit {

  toExchange!: number;
  result!: string;

  title: string = "Exchange your MDL to Euro";

  myForm!: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.myForm = this.fb.group({
      amount: [null,[
        Validators.required,
        Validators.min(1)
      ]]
    });
  }
  get amount(){
    return this.myForm.get('amount');
  }

  changeMoney(){
    if(this.title[24]==='o'){
      this.result= (this.toExchange/21.5).toFixed(2) + ' euro';
    }else{
      this.result= (this.toExchange*21.5).toFixed(2) + ' MDL';
    }
  }

  reset(){
    this.result='';
    this.toExchange = 0;
  }

  swap(){
    if(this.title[24]==='o'){
      this.title = "Exchange your Euro to MDL"
    }else{
      this.title = "Exchange your MDL to Euro"
    }
    this.result='';
    this.toExchange = 0;
  }
}
