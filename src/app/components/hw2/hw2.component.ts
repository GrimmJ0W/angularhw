import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-hw2',
  templateUrl: './hw2.component.html',
  styleUrls: ['./hw2.component.css']
})
export class Hw2Component implements OnInit {

  myForm!: FormGroup;

  name = '';
  age = '';
  email = '';

  list = [{
    name:'',
    age:'',
    email: '',
  }];

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      name:['',[
        Validators.required,
        Validators.minLength(3)
      ]],
      age:[null,[
        Validators.required,
        Validators.min(1)
      ]],
      email: ['',[
        Validators.required,
        Validators.email
      ]]
    })
    this.myForm.valueChanges.subscribe()
  }

  get e_mail(){
    return this.myForm.get('email');
  }
  get user_name(){
    return this.myForm.get('name');
  }
  get user_age(){
    return this.myForm.get('age');
  }

  addUser (){
    if(this.e_mail?.invalid || this.user_name?.invalid || this.user_age?.invalid){
      alert("You used incorrect data or you do not set all fields!")
    }else{
      this.list.push({name: this.name, age: this.age, email: this.email})
    }

  }
}
